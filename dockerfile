# Create image based on the official Node 6 image from dockerhub
FROM node:8.9.3

# Create a directory where our app will be placed
RUN mkdir -p /usr/src/app

# Get all the code needed to run the app
COPY . /usr/src/app

# Change directory so that our commands run inside this new directory
WORKDIR /usr/src/app/server

# Client-side
RUN cd ../client && npm install
RUN cd ../client && ng build

# Server-side
RUN npm install

# Expose the port the app runs in
EXPOSE 4000

# Serve the app
CMD ["npm", "start"]
